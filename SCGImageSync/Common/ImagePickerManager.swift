import Foundation
import UIKit
import PhotosUI

class ImagePickerManager: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var picker = UIImagePickerController()
    var alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    var viewController: UIViewController?
    var pickImageCallback: ((String, String, UIImage) -> Void)?
    
    override init() {
        super.init()
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { _ in
            self.openCamera()
        }
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { _ in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(galleryAction)
        alert.addAction(cancelAction)
    }
    
    func pickImage(_ viewController: UIViewController, _ callback: @escaping ((String, String, UIImage) -> Void)) {
        pickImageCallback = callback
        self.viewController = viewController
        
        alert.popoverPresentationController?.sourceView = self.viewController!.view
        
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        alert.dismiss(animated: true, completion: nil)
        if UIImagePickerController .isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            self.viewController!.present(picker, animated: true, completion: nil)
        } else {
            let alertWarning = UIAlertController(title: "Sorry", message: "Cannot access the camera", preferredStyle: .alert)
            alertWarning.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.viewController?.present(alertWarning, animated: true, completion: nil)
        }
    }
    
    func openGallery() {
        //        var config = PHPickerConfiguration(photoLibrary: PHPhotoLibrary.shared())
        //        config.selectionLimit = 1
        //        config.filter = .images
        //        config.preferredAssetRepresentationMode = .current
        //        let phPicker = PHPickerViewController(configuration: config)
        //        phPicker.delegate = self
        //        self.viewController!.present(phPicker, animated: true, completion: nil)
        alert.dismiss(animated: true, completion: nil)
        picker.sourceType = .photoLibrary
        self.viewController!.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        if let url = info[UIImagePickerController.InfoKey.imageURL] as? URL {
            let fileName = url.lastPathComponent
            let fileType = url.pathExtension
            pickImageCallback?(fileName, fileType, image)
        } else {
            pickImageCallback?(String(Date().timeIntervalSince1970 * 1000), "jpg", image)
        }
    }
}
//
// extension ImagePickerManager: PHPickerViewControllerDelegate {
//    @available(iOS 14, *)
//    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
//        picker.dismiss(animated: true, completion: nil)
//        guard !results.isEmpty else { return }
//
//        if results.count == 1 {
//            let result = results[0]
//
//            var fileName = ""
//            var fileType = ""
//            var image: UIImage!
//
//            let taskGroup = DispatchGroup()
//
//            taskGroup.enter()
//            result.itemProvider.loadFileRepresentation(forTypeIdentifier: "public.item") { (url, error) in
//                if let error = error {
//                    print(error)
//                } else {
//                    if let url = url {
//                        fileName = url.lastPathComponent
//                        fileType = url.pathExtension
//                    }
//                }
//                taskGroup.leave()
//            }
//
//            taskGroup.enter()
//            result.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { (object, error) in
//                if let error = error {
//                    print(error)
//                } else if let temp = object as? UIImage {
//                    image = temp
//                }
//                taskGroup.leave()
//            })
//
//            taskGroup.notify(queue: DispatchQueue.main, work: DispatchWorkItem(block: {
//                self.pickImageCallback?(fileName, fileType, image)
//            }))
//        }
//    }
// }
