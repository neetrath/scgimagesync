import Foundation
import UIKit
import SystemConfiguration

class Utility {
    
    public static func showErrorAlert(message: String, viewController: UIViewController, completion: (() -> Void)?) {
        DispatchQueue.main.async {
            if !message.isEmpty {
                let alert = UIAlertController(title: "Sorry", message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_) in
                    if let okAction = completion {
                        okAction()
                    }
                })
                alert.addAction(okAction)
                viewController.present(alert, animated: true, completion: nil)
            } else {
                if let okAction = completion {
                    okAction()
                }
            }
        }
    }
    
    public class func showAckAlert(title: String, message: String, viewController: UIViewController) {
        self.showAckAlert(title: title, message: message, okText: "OK", okStyle: .default, viewController: viewController, completion: nil)
    }
    
    public class func showAckAlert(title: String, message: String, viewController: UIViewController, completion: (() -> Void)?) {
        self.showAckAlert(title: title, message: message, okText: "OK", okStyle: .default, viewController: viewController, completion: completion)
    }
    
    public class func showAckAlert(title: String, message: String, okText: String, okStyle: UIAlertAction.Style, viewController: UIViewController, completion: (() -> Void)?) {
        DispatchQueue.main.async {
            if !message.isEmpty {
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                let okAction = UIAlertAction(title: okText, style: okStyle, handler: { (_) in
                    if let okAction = completion {
                        okAction()
                    }
                })
                alert.addAction(okAction)
                
                viewController.present(alert, animated: true, completion: nil)
            } else {
                if let okAction = completion {
                    okAction()
                }
            }
        }
    }
    
    public class func showConfirm(title: String, message: String, viewController: UIViewController, okText: String, okStyle: UIAlertAction.Style, okAction: (() -> Void)?) {
        showConfirm(title: title, message: message, viewController: viewController, okText: okText, okStyle: okStyle, cancelText: "Cancel", cancelStyle: .cancel, okAction: okAction, cancelAction: nil)
    }
    
    public class func showConfirm(title: String, message: String, viewController: UIViewController, okText: String, okStyle: UIAlertAction.Style, cancelText: String, cancelStyle: UIAlertAction.Style, okAction: (() -> Void)?, cancelAction: (() -> Void)?) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: okText, style: okStyle, handler: { (_) in
                if let aOkAction = okAction {
                    aOkAction()
                }
            })
            alert.addAction(okAction)
            
            let cancelAction = UIAlertAction(title: cancelText, style: cancelStyle, handler: { (_) in
                if let aCancelAction = cancelAction {
                    aCancelAction()
                }
            })
            alert.addAction(cancelAction)
            
            viewController.present(alert, animated: true, completion: nil)
        }
    }
    
    public static func createSmallImage(imageData: Data?) -> UIImage? {
        if let data = imageData, let image: UIImage = UIImage(data: data) {
            let targetSize = CGSize(width: 40, height: 40)
            return image.resizeImage(targetSize: targetSize)
        } else {
            return nil
        }
    }
    
    public static func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    public static func addGradient(targetView: UIView) {
        let deviceSize: CGRect = UIScreen.main.fixedCoordinateSpace.bounds
        
        let view = UIView(frame: targetView.frame)
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: deviceSize.height, height: view.frame.size.height)
        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradient.locations = [0.6, 1.0]
        view.layer.insertSublayer(gradient, at: 0)
        targetView.addSubview(view)
        targetView.bringSubviewToFront(view)
    }
    
}
