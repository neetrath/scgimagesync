import Foundation

class GlobalVar {
    static let sharedInstance = GlobalVar()
    
    var userId = ""
    
    init() {
        userId = "Anonymous"
    }
}
