import Foundation
import RealmSwift

class UploadingImage: Object {
    
    @objc dynamic var timestamp: Int64 = 0
    @objc dynamic var userId: String = ""
    @objc dynamic var fileName: String = ""
    @objc dynamic var fileType: String = ""
    @objc dynamic var imageData: Data?
    
    override static func primaryKey() -> String? {
        return "timestamp"
    }
}
