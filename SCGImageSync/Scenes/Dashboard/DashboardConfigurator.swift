import Foundation

extension DashboardViewController: DashboardPresenterOutput {
}

extension DashboardPresenter: DashboardInteractorOutput {
}

class DashboardConfigurator {
    static let sharedInstance: DashboardConfigurator = { DashboardConfigurator() }()
    
    func configure(viewController: DashboardViewController) {
        let interactor = DashboardInteractor()
        let presenter = DashboardPresenter()
        let router = DashboardRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.output = viewController
        router.viewController = viewController
    }
}
