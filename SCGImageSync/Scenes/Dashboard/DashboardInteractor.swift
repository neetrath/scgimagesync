import UIKit

protocol DashboardInteractorInput {
    func fetchUploadedImage(request: Dashboard.ViewUploadedCount.Request)
    
    func deleteUploadingImage(request: Dashboard.DeleteUpload.Request)
    func addUploadingImage(request: Dashboard.AddUpload.Request)
    func fetchUploadingImage(request: Dashboard.ViewUploading.Request)
    
    func startUploadImage(request: Dashboard.StartUpload.Request)
}

protocol DashboardInteractorOutput {
    func presentUploadedImageCount(response: Dashboard.ViewUploadedCount.Response)
    
    func presentError(error: Error)
    func presentUploadingImage(response: Dashboard.ViewUploading.Response)
    
    func presentAfterUpload()
}

class DashboardInteractor {
    var presenter: DashboardPresenterInput?
    var output: DashboardInteractorOutput!
    var worker: DashboardWorker = DashboardWorker()
    var commonWorker: CommonWorker = CommonWorker()
}

extension DashboardInteractor: DashboardInteractorInput {
    
    func fetchUploadedImage(request: Dashboard.ViewUploadedCount.Request) {
        worker.fetchUploadedImageCount(userId: request.userId) { [weak self] result in
            switch result {
            case .success(let data):
                let response = Dashboard.ViewUploadedCount.Response(count: data)
                self?.presenter?.presentUploadedImageCount(response: response)
            case .failure(let error):
                self?.presenter?.presentError(error: error)
            }
        }
    }
    
    func startUploadImage(request: Dashboard.StartUpload.Request) {
        commonWorker.fetchUploadingImages(userId: request.userId) { [weak self] result in
            switch result {
            case .success(let images):
                self?.afterFetchImageForUploadSuccess(images: images)
            case .failure: break
            }
        }
    }
    
    private func afterFetchImageForUploadSuccess(images: [UploadingImage]) {
        worker.startUploadImages(images: images) { [weak self] _ in
            self?.presenter?.presentAfterUpload()
        }
    }
    
    func deleteUploadingImage(request: Dashboard.DeleteUpload.Request) {
        worker.deleteUploadingImage(image: request.uploadingImage, reload: true) { [weak self] result in
            switch result {
            case .success(let data):
                let response = Dashboard.ViewUploading.Response(uploadingImages: data, state: "delete")
                self?.presenter?.presentUploadingImage(response: response)
            case .failure(let error):
                self?.presenter?.presentError(error: error)
            }
        }
    }
    
    func addUploadingImage(request: Dashboard.AddUpload.Request) {
        worker.addUploadingImage(image: request.uploadingImage) { [weak self] result in
            switch result {
            case .success(let data):
                let response = Dashboard.ViewUploading.Response(uploadingImages: data, state: "add")
                self?.presenter?.presentUploadingImage(response: response)
            case .failure(let error):
                self?.presenter?.presentError(error: error)
            }
        }
    }
    
    func fetchUploadingImage(request: Dashboard.ViewUploading.Request) {
        commonWorker.fetchUploadingImages(userId: request.userId) { [weak self] result in
            switch result {
            case .success(let data):
                let response = Dashboard.ViewUploading.Response(uploadingImages: data, state: request.state)
                self?.presenter?.presentUploadingImage(response: response)
            case .failure(let error):
                self?.presenter?.presentError(error: error)
            }
        }
    }
}
