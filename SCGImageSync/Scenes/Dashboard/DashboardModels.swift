import UIKit

enum Dashboard {
    
    enum ViewUploading {
        struct Request {
            var userId: String
            var state: String
        }
        struct Response {
            var uploadingImages: [UploadingImage]
            var state: String
        }
        struct ViewModel {
            struct FormattedImage {
                var userId: String
                var fileName: String
                var fileType: String
                var smallImage: UIImage
            }
            var formattedImages: [FormattedImage]
        }
    }
    
    enum AddUpload {
        struct Request {
            var uploadingImage: UploadingImage
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    
    enum DeleteUpload {
        struct Request {
            var uploadingImage: UploadingImage
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    
    enum StartUpload {
        struct Request {
            var userId: String
        }
        struct Response {
        }
        struct ViewModel {
        }
    }
    
    enum ViewUploadedCount {
        struct Request {
            var userId: String
        }
        struct Response {
            var count: Int
        }
        struct ViewModel {
            var count: Int
        }
    }
    
    enum ViewUploaded {
        struct Request {
            var userId: String
        }
        struct Response {
            var timestamp: Int64
            var userId: String
            var fileName: String
            var fileType: String
            var imageData: Data?
        }
        struct ViewModel {
            var fileName: String
            var fileType: String
            var image: UIImage
        }
    }
}
