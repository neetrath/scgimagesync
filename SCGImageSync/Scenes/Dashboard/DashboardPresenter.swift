import UIKit

protocol DashboardPresenterInput {
    func presentUploadedImageCount(response: Dashboard.ViewUploadedCount.Response)
    
    func presentError(error: Error)
    func presentUploadingImage(response: Dashboard.ViewUploading.Response)
    
    func presentAfterUpload()
}

protocol DashboardPresenterOutput: AnyObject {
    func displayUploadedImageCount(viewModel: Dashboard.ViewUploadedCount.ViewModel)
    
    func displayError(message: String)
    func displayUploadingImage(viewModel: Dashboard.ViewUploading.ViewModel)
    
    func displayAfterUpload()
}

class DashboardPresenter {
    weak var output: DashboardPresenterOutput!
}

extension DashboardPresenter: DashboardPresenterInput {
    func presentUploadedImageCount(response: Dashboard.ViewUploadedCount.Response) {
        output.displayUploadedImageCount(viewModel: Dashboard.ViewUploadedCount.ViewModel(count: response.count))
    }
    
    func presentAfterUpload() {
        output.displayAfterUpload()
    }
    
    func presentError(error: Error) {
        if let localDbError = error as? LocalDatabaseError {
            switch localDbError {
            case .decodingError:
                print("Cannot decoding local data")
            case .noDataError:
                print("No data")
            case .readError:
                print("Read error")
            case .deleteError:
                print("Cannot delete local data")
            case .addError:
                print("Cannot add local data")
            }
        }
    }
    
    func presentUploadingImage(response: Dashboard.ViewUploading.Response) {
        var formattedImages: [Dashboard.ViewUploading.ViewModel.FormattedImage] = []
        
        for content in response.uploadingImages {
            let smallImage = Utility.createSmallImage(imageData: content.imageData)
            let temp = Dashboard.ViewUploading.ViewModel.FormattedImage(userId: content.userId, fileName: content.fileName, fileType: content.fileType, smallImage: smallImage ?? Constant.defaultImage)
            formattedImages.append(temp)
        }
        output.displayUploadingImage(viewModel: Dashboard.ViewUploading.ViewModel(formattedImages: formattedImages))
    }
}
