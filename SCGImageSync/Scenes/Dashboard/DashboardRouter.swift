import UIKit

class DashboardRouter {
    
    weak var viewController: DashboardViewController?
    
    // MARK: Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
        
//        if segue.identifier == "seegViewAll" {
//            if let _ = segue.destination as? ListImagesViewController {
//                print("Go to view all screen")
//            }
//        } else if segue.identifier == "segSettings" {
//            if let _ = segue.destination as? SettingsTableViewController {
//                print("Go to settings screen")
//            }
//        }
    }
}
