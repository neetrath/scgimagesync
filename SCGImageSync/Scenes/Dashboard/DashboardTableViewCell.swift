import UIKit

class DashboardTableViewCell: UITableViewCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var smallImageView: UIImageView!
    @IBOutlet weak var fileNameLabel: UILabel!
    @IBOutlet weak var fileTypeLabel: UILabel!
    
    // MARK: Variables
    var imageContent: Dashboard.ViewUploading.ViewModel.FormattedImage? {
        didSet {
            if let content = imageContent {
                fileNameLabel.text = content.fileName
                fileTypeLabel.text = "Type: \(content.fileType)"
                smallImageView.image = content.smallImage
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.selectionStyle = .none
    }

}
