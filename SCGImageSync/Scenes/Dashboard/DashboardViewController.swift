import UIKit
import Reachability
import MBProgressHUD
import FirebaseUI

protocol DashboardViewControllerInput {
    func displayUploadedImageCount(viewModel: Dashboard.ViewUploadedCount.ViewModel)
    
    func displayError(message: String)
    func displayUploadingImage(viewModel: Dashboard.ViewUploading.ViewModel)
    
    func displayAfterUpload()
}

protocol DashboardViewControllerOutput {
    func fetchUploadedImage(request: Dashboard.ViewUploading.Request)
    
    func fetchUploadingImage(request: Dashboard.ViewUploading.Request)
    func addUploadingImage(request: Dashboard.AddUpload.Request)
    func deleteUploadingImage(request: Dashboard.DeleteUpload.Request)
    
    func startUploadImages(request: Dashboard.StartUpload.Request)
}

class DashboardViewController: UIViewController {
    var interactor: DashboardInteractorInput?
    var router: DashboardRouter?
    var output: DashboardViewControllerOutput!
    
    private var formattedImages: [Dashboard.ViewUploading.ViewModel.FormattedImage] = []
    private var hud: MBProgressHUD!

    var reachability: Reachability?
    private var isUploading: Bool = false
    
    // MARK: Outlets
    
    @IBOutlet weak var topUploadingCountLabel: UILabel!
    @IBOutlet weak var topUploadedCountLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var uploadingCountLabel: UILabel!
    @IBOutlet weak var onlineStatusLabel: UILabel!
    @IBOutlet weak var signInButton: UIBarButtonItem!
    @IBOutlet weak var settingArea: UIView!
    @IBOutlet weak var viewAllImageArea: UIView!
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        DashboardConfigurator.sharedInstance.configure(viewController: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        DashboardConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let router = router {
            router.passDataToNextScene(segue: segue)
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let user = Auth.auth().currentUser
        if let user = user {
            GlobalVar.sharedInstance.userId = user.uid
            signInButton.isHidden = true
            self.navigationItem.title = user.displayName!
        } else {
            GlobalVar.sharedInstance.userId = "Anonymous"
            signInButton.isHidden = false
            self.navigationItem.title = "Dashboard"
        }
        
        let uploadingRequest = Dashboard.ViewUploading.Request(userId: GlobalVar.sharedInstance.userId, state: "read")
        interactor?.fetchUploadingImage(request: uploadingRequest)
        
        let uploadedRequest = Dashboard.ViewUploadedCount.Request(userId: GlobalVar.sharedInstance.userId)
        interactor?.fetchUploadedImage(request: uploadedRequest)
    }
    
    private func setupView() {
        let allImageGesture = UITapGestureRecognizer(target: self, action: #selector (self.navigateToViewAll (_:)))
        self.viewAllImageArea.addGestureRecognizer(allImageGesture)
        
        let settingGesture = UITapGestureRecognizer(target: self, action: #selector (self.navigateToSettings (_:)))
        self.settingArea.addGestureRecognizer(settingGesture)
        
        self.uploadingCountLabel.text = "Support jpg, jpeg, png, heic file only"
        self.tableView.backgroundColor = UIColor.white
        
        reachability = try? Reachability()
        reachability?.whenReachable = { _ in
            self.onlineStatusLabel.text = "Online"
            self.onlineStatusLabel.textColor = .green
            
            if !self.isUploading {
                self.isUploading = true
                let request = Dashboard.StartUpload.Request(userId: GlobalVar.sharedInstance.userId)
                self.interactor?.startUploadImage(request: request)
            }
        }
        reachability?.whenUnreachable = { _ in
            self.onlineStatusLabel.text = "Offline"
            self.onlineStatusLabel.textColor = .red
            
            self.topUploadedCountLabel.text = "N/A"
        }
        
        do {
            try reachability?.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    // MARK: IB Actions
    
    @IBAction func signInAction(_ sender: UIBarButtonItem) {
        if let authUI = FUIAuth.defaultAuthUI() {
            authUI.delegate = self
            
            let providers: [FUIAuthProvider] = [FUIGoogleAuth(), FUIEmailAuth()]
            authUI.providers = providers
            
            let authViewController = authUI.authViewController()
            authViewController.modalPresentationStyle = .fullScreen
            self.present(authViewController, animated: true, completion: nil)
        }
    }
    
    @objc func navigateToViewAll(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "segViewAll", sender: nil)
    }
    
    @objc func navigateToSettings(_ sender: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "segSettings", sender: nil)
    }
    
    // MARK: Pick image
    
    @IBAction func pickImageAction(_ sender: UIButton) {
        ImagePickerManager().pickImage(self) { fileName, fileType, image in
            print(fileName)
            if fileType.lowercased().contains("png") || fileType.lowercased().contains("jpg") || fileType.lowercased().contains("jpeg") || fileType.lowercased().contains("heic") {
                
                if self.formattedImages.count > Constant.uploadingMax - 1 {
                    Utility.showErrorAlert(message: "Maximum number of pending upload is \(Constant.uploadingMax)", viewController: self, completion: nil)
                    return
                }
                
                if let view = self.view {
                    self.hud = MBProgressHUD.showAdded(to: view, animated: true)
                    self.hud.label.text = "Optimizing Image..."
                }
                
                var compressedImage: UIImage!
                DispatchQueue.background(background: {
                    compressedImage = image.resizedTo1MB()
                }, completion: {
                    self.hud.hide(animated: true)
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    let newFileName = "\(formatter.string(from: Date())).\(fileType)"
                    
                    let uploadingImage = UploadingImage()
                    uploadingImage.userId = GlobalVar.sharedInstance.userId
                    uploadingImage.fileName = newFileName
                    uploadingImage.fileType = fileType
                    uploadingImage.imageData = compressedImage.pngData()
                    
                    let request = Dashboard.AddUpload.Request(uploadingImage: uploadingImage)
                    self.interactor?.addUploadingImage(request: request)
                })
            } else {
                Utility.showErrorAlert(message: "Unsupported file type", viewController: self, completion: nil)
            }
        }
    }
}

extension DashboardViewController: DashboardViewControllerInput {
    
    func displayUploadedImageCount(viewModel: Dashboard.ViewUploadedCount.ViewModel) {
        topUploadedCountLabel.text = String(viewModel.count)
    }
    
    func displayAfterUpload() {
        isUploading = false
        
        let uploadingRequest = Dashboard.ViewUploading.Request(userId: GlobalVar.sharedInstance.userId, state: "uploaded")
        interactor?.fetchUploadingImage(request: uploadingRequest)
        
        let uploadedRequest = Dashboard.ViewUploadedCount.Request(userId: GlobalVar.sharedInstance.userId)
        interactor?.fetchUploadedImage(request: uploadedRequest)
    }
    
    func displayError(message: String) {
        Utility.showErrorAlert(message: message, viewController: self, completion: nil)
    }
    
    func displayUploadingImage(viewModel: Dashboard.ViewUploading.ViewModel) {
        formattedImages = viewModel.formattedImages
//        uploadingCountLabel.text = "\(formattedImages.count) / \(uploadingMax)"
        
        if let label = self.topUploadingCountLabel {
            label.text = String(formattedImages.count)
        }
        
        if let tableView = self.tableView {
            tableView.reloadData()
        }
        
        if formattedImages.count > 0 && !isUploading {
            isUploading = true
            let request = Dashboard.StartUpload.Request(userId: GlobalVar.sharedInstance.userId)
            interactor?.startUploadImage(request: request)
        }
    }
}

extension DashboardViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formattedImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "uploadingCell", for: indexPath) as? DashboardTableViewCell else {
            return UITableViewCell()
        }
        cell.imageContent = formattedImages[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let formattedImage = formattedImages[indexPath.row]
            
            let image = UploadingImage()
            image.userId = formattedImage.userId
            image.fileName = formattedImage.fileName
            image.fileType = formattedImage.fileType
            
            let request = Dashboard.DeleteUpload.Request(uploadingImage: image)
            interactor?.deleteUploadingImage(request: request)
        }
    }
}

extension DashboardViewController: FUIAuthDelegate {
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        if let user = authDataResult?.user {
            GlobalVar.sharedInstance.userId = user.uid
            signInButton.isHidden = true
            self.navigationItem.title = user.displayName!
        } else {
            GlobalVar.sharedInstance.userId = "Anonymous"
            signInButton.isHidden = false
            self.navigationItem.title = "Dashboard"
        }
    }
}
