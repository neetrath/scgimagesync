import UIKit

class DashboardWorker {
    
    func addUploadingImage(image: UploadingImage, completionHandler: @escaping (Result<[UploadingImage], LocalDatabaseError>) -> Void) {
        var uploadingImages: [UploadingImage] = []
        uploadingImages.append(image)
        
        UploadLocal.addUploadingImagesToRealm(images: uploadingImages) { result in
            switch result {
            case .success:
                // Reload after add
                UploadLocal.fetchUploadingImageFromRealm(userId: image.userId) { result in
                    switch result {
                    case .success:
                        DispatchQueue.main.async {
                            completionHandler(result)
                        }
                    case .failure:
                        DispatchQueue.main.async {
                            completionHandler(result)
                        }
                    }
                }
            case .failure:
                completionHandler(.failure(.addError))
            }
        }
    }
    
    func deleteUploadingImage(image: UploadingImage, reload: Bool, completionHandler: @escaping (Result<[UploadingImage], LocalDatabaseError>) -> Void) {
        UploadLocal.deleteUploadingImageFromRealm(image: image, completionHandler: { result in
            switch result {
            case .success:
                if reload {
                    // Reload if single image delete, Call from table view
                    UploadLocal.fetchUploadingImageFromRealm(userId: image.userId) { result in
                        switch result {
                        case .success:
                            DispatchQueue.main.async {
                                completionHandler(result)
                            }
                        case .failure:
                            DispatchQueue.main.async {
                                completionHandler(result)
                            }
                        }
                    }
                } else {
                    // Don't reload if call from startUploadImages()
                    DispatchQueue.main.async {
                        completionHandler(.success([]))
                    }
                }
            case .failure:
                completionHandler(.failure(.deleteError))
            }
        })
    }
    
    func startUploadImages(images: [UploadingImage], completionHandler: @escaping (Result<String, LocalDatabaseError>) -> Void) {
        let taskGroup = DispatchGroup()
        
        for image in images {
            taskGroup.enter()
            UploadAPI.uploadImageToFirebase(image: image) { [weak self] _ in
                self?.deleteUploadingImage(image: image, reload: false) { _ in
                    taskGroup.leave()
                }
            }
        }
        
        taskGroup.notify(queue: DispatchQueue.main, work: DispatchWorkItem(block: {
            completionHandler(.success(""))
        }))
    }
    
    func fetchUploadedImageCount(userId: String, completionHandler: @escaping (Result<Int, FirebaseError>) -> Void) {
        UploadAPI.fetchImageCountFromFirebase(userId: userId, completionHandler: completionHandler)
    }
}
