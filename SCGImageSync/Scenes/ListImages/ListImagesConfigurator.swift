import Foundation

extension ListImagesViewController: ListImagesPresenterOutput {
}

extension ListImagesPresenter: ListImagesInteractorOutput {
}

class ListImagesConfigurator {
    static let sharedInstance: ListImagesConfigurator = { ListImagesConfigurator() }()
    
    func configure(viewController: ListImagesViewController) {
        let interactor = ListImagesInteractor()
        let presenter = ListImagesPresenter()
        let router = ListImagesRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.output = viewController
        router.viewController = viewController
    }
}
