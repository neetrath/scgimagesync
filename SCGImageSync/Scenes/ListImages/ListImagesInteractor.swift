import UIKit

protocol ListImagesInteractorInput {
    func fetchAllImage(request: ListImages.ViewAll.Request)
}

protocol ListImagesInteractorOutput {
    func presentAllImage(response: ListImages.ViewAll.Response)
    func presentError(error: Error)
}

class ListImagesInteractor {
    var presenter: ListImagesPresenterInput?
    var output: ListImagesInteractorOutput!
    var worker: ListImagesWorker = ListImagesWorker()
    var commonWorker: CommonWorker = CommonWorker()
}

extension ListImagesInteractor: ListImagesInteractorInput {
    func fetchAllImage(request: ListImages.ViewAll.Request) {
        commonWorker.fetchAllImages(userId: request.userId, isOnline: request.isOnline) { result in
            switch result {
            case .success(let response):
                self.presenter?.presentAllImage(response: response)
            case .failure:
                break
            }
        }
    }
}
