import UIKit

enum ListImages {
    enum ViewAll {
        struct Request {
            var userId: String
            var isOnline: Bool
        }
        struct Response {
            var uploadingImages: [UploadingImage]
            var uploadedImages: [Dashboard.ViewUploaded.Response]
        }
        struct ViewModel {
            struct FormattedImage {
                var timestamp: Int64
                var userId: String
                var fileName: String
                var fileType: String
                var status: String
                var smallImage: UIImage
                var largeImage: UIImage
            }
            var formattedImages: [FormattedImage]
        }
    }
}
