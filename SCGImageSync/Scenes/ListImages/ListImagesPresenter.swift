import UIKit

protocol ListImagesPresenterInput {
    func presentAllImage(response: ListImages.ViewAll.Response)
    func presentError(error: Error)
}

protocol ListImagesPresenterOutput: AnyObject {
    func displayAllImage(viewModel: ListImages.ViewAll.ViewModel)
    func displayError(message: String)
}

class ListImagesPresenter {
    weak var output: ListImagesPresenterOutput!
}

extension ListImagesPresenter: ListImagesPresenterInput {
    func presentAllImage(response: ListImages.ViewAll.Response) {
        let uploadingImages = response.uploadingImages
        let uploadedImages = response.uploadedImages
        
        var formattedImages: [ListImages.ViewAll.ViewModel.FormattedImage] = []
        
        for content in uploadingImages {
            let smallImage = Utility.createSmallImage(imageData: content.imageData)
            let largeImage = UIImage(data: content.imageData!)
            let temp = ListImages.ViewAll.ViewModel.FormattedImage(timestamp: content.timestamp, userId: content.userId, fileName: content.fileName, fileType: content.fileType, status: "Offline", smallImage: smallImage ?? Constant.defaultImage, largeImage: largeImage ?? Constant.defaultImage)
            formattedImages.append(temp)
        }
        
        for content in uploadedImages {
            let smallImage = Utility.createSmallImage(imageData: content.imageData)
            let largeImage = UIImage(data: content.imageData!)
            let temp = ListImages.ViewAll.ViewModel.FormattedImage(timestamp: content.timestamp, userId: content.userId, fileName: content.fileName, fileType: content.fileType, status: "Online", smallImage: smallImage ?? Constant.defaultImage, largeImage: largeImage ?? Constant.defaultImage)
            formattedImages.append(temp)
        }
        
        formattedImages.sort { $0.fileName > $1.fileName }
        
        let viewModel = ListImages.ViewAll.ViewModel(formattedImages: formattedImages)
        output.displayAllImage(viewModel: viewModel)
    }

    func presentError(error: Error) {
        if let localDbError = error as? LocalDatabaseError {
            switch localDbError {
            case .decodingError:
                print("Cannot decoding local data")
            case .noDataError:
                print("No data")
            case .readError:
                print("Read error")
            case .deleteError:
                print("Cannot delete local data")
            case .addError:
                print("Cannot add local data")
            }
        }
    }
}
