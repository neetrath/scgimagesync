import UIKit

class ListImagesRouter {
    weak var viewController: ListImagesViewController?
    
    // MARK: Communication
    
    func passDataToNextScene(segue: UIStoryboardSegue) {
        // NOTE: Teach the router which scenes it can communicate with
        
//        if segue.identifier == "segSettings" {
//            if let settingsVC = segue.destination as? SettingsTableViewController {
//                print("Go to settings screen")
//            }
//        }
    }
}
