import UIKit

class ListImagesTableViewCell: UITableViewCell {

    // MARK: Outlets
    
    @IBOutlet weak var smallImageView: UIImageView!
    @IBOutlet weak var fileNameLabel: UILabel!
    @IBOutlet weak var fileTypeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    // MARK: Variables
    var imageContent: ListImages.ViewAll.ViewModel.FormattedImage? {
        didSet {
            if let content = imageContent {
                fileNameLabel.text = content.fileName
                fileTypeLabel.text = "Type: \(content.fileType)"
                smallImageView.image = content.smallImage
                statusLabel.text = content.status
                
                if content.status == "Online" {
                    statusLabel.textColor = .green
                } else {
                    statusLabel.textColor = .red
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.selectionStyle = .none
    }
}
