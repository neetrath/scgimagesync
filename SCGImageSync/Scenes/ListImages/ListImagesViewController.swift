import UIKit
import Reachability
import MBProgressHUD
import Lightbox

protocol ListImagesViewControllerInput {
    func displayAllImage(viewModel: ListImages.ViewAll.ViewModel)
    func displayError(message: String)
}

protocol ListImagesViewControllerOutput {
    func fetchAllImage(viewModel: ListImages.ViewAll.Request)
}

class ListImagesViewController: UIViewController {
    var interactor: ListImagesInteractorInput?
    var router: ListImagesRouter?
    var output: ListImagesViewControllerOutput!
    
    private var formattedImages: [ListImages.ViewAll.ViewModel.FormattedImage] = []
    private var hud: MBProgressHUD!
    private let refreshControl = UIRefreshControl()
    private var lightboxController: LightboxController!
    private var lightboxImages: [LightboxImage] = []
    
    var reachability: Reachability?
    
    // MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!

    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        ListImagesConfigurator.sharedInstance.configure(viewController: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        ListImagesConfigurator.sharedInstance.configure(viewController: self)
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let router = router {
            router.passDataToNextScene(segue: segue)
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        fetchData()
    }
    
    private func setupView() {
        let backButton = UIBarButtonItem()
        backButton.title = "Back"
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh and check status.", attributes: attributes)
        refreshControl.addTarget(self, action: #selector(didPullToRefreshCommit(_:)), for: .valueChanged)
        
        tableView.refreshControl = refreshControl
    }
    
    @objc
    private func didPullToRefreshCommit(_ sender: Any) {
        fetchData()
    }
    
    private func fetchData() {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        let request = ListImages.ViewAll.Request(userId: GlobalVar.sharedInstance.userId, isOnline: Utility.isConnectedToNetwork())
        interactor?.fetchAllImage(request: request)
    }
}

extension ListImagesViewController: ListImagesViewControllerInput {
    func displayAllImage(viewModel: ListImages.ViewAll.ViewModel) {
        hud?.hide(animated: true)
        formattedImages = viewModel.formattedImages
        
        lightboxImages.removeAll()
        for image in formattedImages {
            lightboxImages.append(LightboxImage(image: image.largeImage))
        }
        
        if self.refreshControl.isRefreshing {
            self.refreshControl.endRefreshing()
        }
        tableView.reloadData()
    }
    
    func displayError(message: String) {
        hud.hide(animated: true)
        Utility.showErrorAlert(message: message, viewController: self, completion: nil)
    }
}

extension ListImagesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return formattedImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "allImageCell", for: indexPath) as? ListImagesTableViewCell else {
            return UITableViewCell()
        }
        cell.imageContent = formattedImages[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        lightboxController = LightboxController(images: lightboxImages, startIndex: indexPath.row)
        lightboxController.dynamicBackground = true
        present(lightboxController, animated: true, completion: nil)
    }
}
