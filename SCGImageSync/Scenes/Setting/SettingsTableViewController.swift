import UIKit
import FirebaseAuth

class SettingsTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backButton = UIBarButtonItem()
        backButton.title = "Back"
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = Auth.auth().currentUser
        if user != nil {
            Utility.showConfirm(title: "Confirm", message: "Are you sure you want to sign out?", viewController: self, okText: "Sign Out", okStyle: .destructive) {
                do {
                    try Auth.auth().signOut()
                    self.navigationController?.popViewController(animated: true)
                } catch {
                    print("already logged out")
                }
            }
        } else {
            Utility.showErrorAlert(message: "Please sign in first", viewController: self) {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
