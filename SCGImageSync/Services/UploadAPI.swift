import Foundation
import Firebase

enum FirebaseError: Error {
    case noMetadata
    case noData
    case unknownError
}

class UploadAPI {
    
    // MARK: - Upload to Firebase
    
    static func uploadImageToFirebase(image: UploadingImage, completionHandler: @escaping (Result<String, FirebaseError>) -> Void) {
        if let data = image.imageData {
            let storage = Storage.storage()
            let storageRef = storage.reference()
            
            // Create a reference to the file you want to upload
            let fileRef = storageRef.child("images/\(image.userId)/\(image.fileName)")
            
            fileRef.putData(data, metadata: nil) { (metadata, error) in
                if let error = error {
                    print(error)
                    completionHandler(.failure(.unknownError))
                    return
                }
                
                guard let metadata = metadata else {
                    completionHandler(.failure(.noMetadata))
                    return
                }
                print(metadata)
                completionHandler(.success(image.fileName))
            }
        }
    }
    
    // MARK: - List from Firebase
    
    static func fetchImageCountFromFirebase(userId: String, completionHandler: @escaping (Result<Int, FirebaseError>) -> Void) {
        let storage = Storage.storage()
        
        let storageReference = storage.reference().child("images/\(userId)")
        storageReference.listAll { (result, error) in
            if let error = error {
                print(error)
                completionHandler(.failure(.unknownError))
                return
            }
            completionHandler(.success(result.items.count))
        }
    }
    
    static func fetchImagesFromFirebase(userId: String, completionHandler: @escaping (Result<[Dashboard.ViewUploaded.Response], FirebaseError>) -> Void) {
        let storage = Storage.storage()
        
        let storageReference = storage.reference().child("images/\(userId)")
        storageReference.listAll { (result, error) in
            if let error = error {
                print(error)
                completionHandler(.failure(.unknownError))
                return
            }
            
            var images: [Dashboard.ViewUploaded.Response] = []
            let taskGroup = DispatchGroup()
            for item in result.items {
                taskGroup.enter()
                getDataFromFirebase(userId: userId, item: item) { result in
                    switch result {
                    case .success(let image):
                        images.append(image)
                    case .failure:
                        print("Some error happened")
                    }
                    taskGroup.leave()
                }
            }
            
            taskGroup.notify(queue: DispatchQueue.main, work: DispatchWorkItem(block: {
                completionHandler(.success(images))
            }))
        }
    }
    
    private static func getDataFromFirebase(userId: String, item: StorageReference, completionHandler: @escaping (Result<Dashboard.ViewUploaded.Response, FirebaseError>) -> Void) {
        let size1MB: Int64 = 1 * 1024 * 1024
        item.getData(maxSize: size1MB) { data, error in
            if let error = error {
                print(error)
                completionHandler(.failure(.noData))
            } else {
                guard let data = data else {
                    completionHandler(.failure(.unknownError))
                    return
                }
                let image = Dashboard.ViewUploaded.Response(timestamp: Int64(item.name) ?? 0, userId: userId, fileName: item.name, fileType: item.name.fileExtension(), imageData: data)
                completionHandler(.success(image))
            }
        }
    }
}
