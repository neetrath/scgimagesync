import Foundation
import RealmSwift

enum LocalDatabaseError: Error {
    case readError
    case noDataError
    case decodingError
    case deleteError
    case addError
}

class UploadLocal {
    
    static func deleteUploadingImageFromRealm(image: UploadingImage, completionHandler: @escaping (Result<String, LocalDatabaseError>) -> Void) {
        do {
            let realm = try Realm()
            
            let predicate = NSPredicate(format: "userId = %@ AND fileName = %@", image.userId, image.fileName)
            if let temp = realm.objects(UploadingImage.self).filter(predicate).first {
                try realm.write {
                    realm.delete(temp)
                }
                completionHandler(.success(""))
            } else {
                completionHandler(.failure(.deleteError))
            }
        } catch {
            print(error)
            completionHandler(.failure(.deleteError))
            return
        }
    }
    
    static func addUploadingImagesToRealm(images: [UploadingImage], completionHandler: @escaping (Result<String, LocalDatabaseError>) -> Void) {
        do {
            let realm = try Realm()
            try realm.write {
                for image in images {
                    image.timestamp = Int64(Date().timeIntervalSince1970 * 1000)
                    realm.add(image, update: .modified)
                }
            }
            completionHandler(.success(""))
        } catch {
            print(error)
            completionHandler(.failure(.addError))
            return
        }
    }
    
    static func fetchUploadingImageFromRealm(userId: String, completionHandler: @escaping (Result<[UploadingImage], LocalDatabaseError>) -> Void) {
        do {
            let realm = try Realm()
            
            let predicate = NSPredicate(format: "userId = %@", userId)
            let temp = realm.objects(UploadingImage.self).filter(predicate).sorted(byKeyPath: "timestamp", ascending: false)
            
            var result: [UploadingImage] = []
            for item in temp {
                result.append(item)
            }
            
            completionHandler(.success(result))
        } catch {
            completionHandler(.failure(.readError))
        }
    }
}
