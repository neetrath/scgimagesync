import Foundation

enum CommonError: Error {

}

class CommonWorker {
    
    func fetchUploadingImages(userId: String, completionHandler: @escaping (Result<[UploadingImage], LocalDatabaseError>) -> Void) {
        UploadLocal.fetchUploadingImageFromRealm(userId: userId) { result in
            switch result {
            case .success:
                DispatchQueue.main.async {
                    completionHandler(result)
                }
            case .failure:
                DispatchQueue.main.async {
                    completionHandler(result)
                }
            }
        }
    }
    
    func fetchUploadedImages(userId: String, completionHandler: @escaping (Result<[Dashboard.ViewUploaded.Response], FirebaseError>) -> Void) {
        UploadAPI.fetchImagesFromFirebase(userId: userId) { result in
            switch result {
            case .success:
                DispatchQueue.main.async {
                    completionHandler(result)
                }
            case .failure:
                DispatchQueue.main.async {
                    completionHandler(result)
                }
            }
        }
    }
    
    func fetchAllImages(userId: String, isOnline: Bool, completionHandler: @escaping (Result<ListImages.ViewAll.Response, CommonError>) -> Void) {
        var uploadingImages: [UploadingImage] = []
        var uploadedImages: [Dashboard.ViewUploaded.Response] = []
        
        let taskGroup = DispatchGroup()
        taskGroup.enter()
        fetchUploadingImages(userId: userId) { result in
            switch result {
            case .success(let images):
                uploadingImages = images
                taskGroup.leave()
            case .failure:
                taskGroup.leave()
            }
        }
        
        if isOnline {
            taskGroup.enter()
            fetchUploadedImages(userId: userId) { result in
                switch result {
                case .success(let images):
                    uploadedImages = images
                    taskGroup.leave()
                case .failure:
                    taskGroup.leave()
                }
            }
        }
        
        taskGroup.notify(queue: DispatchQueue.main, work: DispatchWorkItem(block: {
            let response = ListImages.ViewAll.Response(uploadingImages: uploadingImages, uploadedImages: uploadedImages)
            completionHandler(.success(response))
        }))
    }
}
