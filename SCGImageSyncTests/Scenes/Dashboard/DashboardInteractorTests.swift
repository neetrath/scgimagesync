@testable import SCGImageSync
import XCTest

class DashboardInteractorTests: XCTestCase {
    // MARK: - Subject under test
    
    var sut: DashboardInteractor!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupDashboardInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupDashboardInteractor() {
        sut = DashboardInteractor()
    }
    
    // MARK: - Test doubles
    
    class DashboardPresentationLogicSpy: DashboardPresenterInput {
        
        // MARK: Method call expectations
        
        var presentErrorCalled = false
        var presentFetchedUploadingImageCalled = false
        var presentReloadAfterDeleteCalled = false
        var presentReloadAfterAddCalled = false
        
        // MARK: Spied methods
        
        func presentUploadedImageCount(response: Dashboard.ViewUploadedCount.Response) {
            
        }
        
        func presentError(error: Error) {
            presentErrorCalled = true
        }
        
        func presentUploadingImage(response: Dashboard.ViewUploading.Response) {
            if response.state == "delete" {
                presentReloadAfterDeleteCalled = true
            } else if response.state == "add" {
                presentReloadAfterAddCalled = true
            } else {
                presentFetchedUploadingImageCalled = true
            }
        }
        
        func presentAfterUpload() {
            
        }
    }
    
    class DashboardWorkerSpy: DashboardWorker {
        // MARK: Method call expectations
        
        var reloadAfterDeleteCalled = false
        var reloadAfterAddCalled = false
        // MARK: Spied methods
        
        override func deleteUploadingImage(image: UploadingImage, reload: Bool, completionHandler: @escaping (Result<[UploadingImage], LocalDatabaseError>) -> Void) {
            reloadAfterDeleteCalled = true
            completionHandler(.success([]))
        }
        
        override func addUploadingImage(image: UploadingImage, completionHandler: @escaping (Result<[UploadingImage], LocalDatabaseError>) -> Void) {
            reloadAfterAddCalled = true
            completionHandler(.success([]))
        }
    }
    
    class CommonWorkerSpy: CommonWorker {
        // MARK: Method call expectations
        
        var fetchUploadingImageCalled = false
        
        // MARK: Spied methods
        
        override func fetchUploadingImages(userId: String, completionHandler: @escaping (Result<[UploadingImage], LocalDatabaseError>) -> Void) {
            fetchUploadingImageCalled = true
            
            let uploadingImage = UploadingImage()
            uploadingImage.timestamp = 0
            uploadingImage.userId = "1"
            uploadingImage.fileName = "test.jpg"
            uploadingImage.fileType = "jpg"
            uploadingImage.imageData = Constant.defaultImage.jpegData(compressionQuality: 1.0)
            
            let result: [UploadingImage] = [uploadingImage]
            completionHandler(.success(result))
        }
    }
    
    // MARK: - Tests
    
    func testFetchUploadingImageShouldAskCommonWorkerToFetchAndPresenterToFormatResult() {
        // Given
        let DashboardPresentationLogicSpy = DashboardPresentationLogicSpy()
        sut.presenter = DashboardPresentationLogicSpy
        let commonWorkerSpy = CommonWorkerSpy()
        sut.commonWorker = commonWorkerSpy
        
        // When
        let request = Dashboard.ViewUploading.Request(userId: "1", state: "load")
        sut.fetchUploadingImage(request: request)
        
        // Then
        XCTAssert(commonWorkerSpy.fetchUploadingImageCalled, "Fetch uploading image should ask common worker to fetch local image")
        XCTAssert(DashboardPresentationLogicSpy.presentFetchedUploadingImageCalled, "Fetch uploading image should ask presenter to format result")
    }
    
    func testDeleteUploadingImageShouldAskDashboardWorkerToDeleteAndReload() {
        // Given
        let DashboardPresentationLogicSpy = DashboardPresentationLogicSpy()
        sut.presenter = DashboardPresentationLogicSpy
        let workerSpy = DashboardWorkerSpy()
        sut.worker = workerSpy
        
        let uploadingImage = UploadingImage()
        uploadingImage.timestamp = 0
        uploadingImage.userId = "1"
        uploadingImage.fileName = "test.jpg"
        uploadingImage.fileType = "jpg"
        uploadingImage.imageData = Constant.defaultImage.jpegData(compressionQuality: 1.0)
        
        // When
        let request = Dashboard.DeleteUpload.Request(uploadingImage: uploadingImage)
        sut.deleteUploadingImage(request: request)
        
        // Then
        XCTAssert(workerSpy.reloadAfterDeleteCalled, "Should ask dashboard worker to delete and fetch image again")
        XCTAssert(DashboardPresentationLogicSpy.presentReloadAfterDeleteCalled, "Should reload data after delete")
    }
    
    func testAddUploadingImageShouldAskDashboardWorkerToAddAndReload() {
        // Given
        let DashboardPresentationLogicSpy = DashboardPresentationLogicSpy()
        sut.presenter = DashboardPresentationLogicSpy
        let workerSpy = DashboardWorkerSpy()
        sut.worker = workerSpy
        
        let uploadingImage = UploadingImage()
        uploadingImage.timestamp = 0
        uploadingImage.userId = "1"
        uploadingImage.fileName = "test.jpg"
        uploadingImage.fileType = "jpg"
        uploadingImage.imageData = Constant.defaultImage.jpegData(compressionQuality: 1.0)
        
        // When
        let request = Dashboard.AddUpload.Request(uploadingImage: uploadingImage)
        sut.addUploadingImage(request: request)
        
        // Then
        XCTAssert(workerSpy.reloadAfterAddCalled, "Should ask dashboard worker to add and fetch image again")
        XCTAssert(DashboardPresentationLogicSpy.presentReloadAfterAddCalled, "Should reload data after add")
    }
}
