@testable import SCGImageSync
import XCTest

class DashboardPresenterTests: XCTestCase {
    // MARK: - Subject under test
    
    var sut: DashboardPresenter!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupDashboardPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupDashboardPresenter() {
        sut = DashboardPresenter()
    }
    
    // MARK: - Test doubles
    
    class DashboardDisplayLogicSpy: DashboardPresenterOutput {

        // MARK: Method call expectations
        
        var displayFetchedUploadingImageCalled = false
        var displayErrorCalled = false
        
        // MARK: Argument expectations
        
        var viewModel: Dashboard.ViewUploading.ViewModel!
        var errorMessage: String!
        
        // MARK: Spied methods
        
        func displayUploadedImageCount(viewModel: Dashboard.ViewUploadedCount.ViewModel) {
            
        }
        
        func displayError(message: String) {
            displayErrorCalled = true
            self.errorMessage = message
        }
        
        func displayUploadingImage(viewModel: Dashboard.ViewUploading.ViewModel) {
            displayFetchedUploadingImageCalled = true
            self.viewModel = viewModel
        }
        
        func displayAfterUpload() {
            
        }
    }
    
    // MARK: - Tests

    func testPresentFetchedUploadingImageShouldAskViewControllerToDisplayUploadingImage() {
        // Given
        let dashboardDisplayLogicSpy = DashboardDisplayLogicSpy()
        sut.output = dashboardDisplayLogicSpy

        // When
        
        let uploadingImage = UploadingImage()
        uploadingImage.timestamp = 0
        uploadingImage.userId = "1"
        uploadingImage.fileName = "test.jpg"
        uploadingImage.fileType = "jpg"
        uploadingImage.imageData = Constant.defaultImage.jpegData(compressionQuality: 1.0)
        
        let uploadingImages = [uploadingImage]
        let response = Dashboard.ViewUploading.Response(uploadingImages: uploadingImages, state: "read")
        sut.presentUploadingImage(response: response)

        // Then
        XCTAssert(dashboardDisplayLogicSpy.displayFetchedUploadingImageCalled, "Presenting fetched uploading image should ask view controller to display them")
    }
}
