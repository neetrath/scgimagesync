@testable import SCGImageSync
import XCTest

class DashboardViewControllerTests: XCTestCase {
    // MARK: - Subject under test
    
    var sut: DashboardViewController!
    var window: UIWindow!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupDashboardViewController()
    }
    
    override func tearDown() {
        window = nil
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupDashboardViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    // MARK: - Test doubles
    
    class DashboardBusinessLogicSpy: DashboardInteractorInput {
        
        // MARK: Method call expectations
        
        var fetchUploadingImageCalled = false
        var fetchUploadedImageCalled = false
        
        // MARK: Spied methods
        
        func fetchUploadedImage(request: Dashboard.ViewUploadedCount.Request) {
            fetchUploadedImageCalled = true
        }
        
        func deleteUploadingImage(request: Dashboard.DeleteUpload.Request) {
            
        }
        
        func addUploadingImage(request: Dashboard.AddUpload.Request) {
            
        }
        
        func fetchUploadingImage(request: Dashboard.ViewUploading.Request) {
            fetchUploadingImageCalled = true
        }
        
        func startUploadImage(request: Dashboard.StartUpload.Request) {
            
        }
    }
    
    class TableViewSpy: UITableView {
        // MARK: Method call expectations
        
        var reloadDataCalled = false
        
        // MARK: Spied methods
        
        override func reloadData() {
            reloadDataCalled = true
        }
    }
    
    // MARK: - Tests
    
    func testShouldFetchUploadingImageWhenViewWillAppear() {
        // Given
        let DashboardBusinessLogicSpy = DashboardBusinessLogicSpy()
        sut.interactor = DashboardBusinessLogicSpy
        loadView()
        
        // When
        sut.viewWillAppear(true)
        
        // Then
        XCTAssert(DashboardBusinessLogicSpy.fetchUploadingImageCalled, "Should fetch values when view will appear")
    }
    
    func testShouldFetchUploadedImageWhenViewWillAppear() {
        // Given
        let DashboardBusinessLogicSpy = DashboardBusinessLogicSpy()
        sut.interactor = DashboardBusinessLogicSpy
        loadView()
        
        // When
        sut.viewWillAppear(true)
        
        // Then
        XCTAssert(DashboardBusinessLogicSpy.fetchUploadedImageCalled, "Should fetch values when view will appear")
    }
    
    func testShouldDisplayFetchedUploadingImage() {
        // Given
        let tableViewSpy = TableViewSpy()
        sut.tableView = tableViewSpy
        
        // When
        let displayValues = [Dashboard.ViewUploading.ViewModel.FormattedImage(userId: "1", fileName: "Test.jpg", fileType: "jpg", smallImage: Constant.defaultImage)]
        let viewModel = Dashboard.ViewUploading.ViewModel(formattedImages: displayValues)
        sut.displayUploadingImage(viewModel: viewModel)
        
        // Then
        XCTAssert(tableViewSpy.reloadDataCalled, "Displaying fetched values should reload the table view")
    }
}
