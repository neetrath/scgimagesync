@testable import SCGImageSync
import XCTest

class ListImagesInteractorTests: XCTestCase {
    // MARK: - Subject under test
    
    var sut: ListImagesInteractor!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupListImagesInteractor()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupListImagesInteractor() {
        sut = ListImagesInteractor()
    }
    
    // MARK: - Test doubles
    
    class ListImagesPresentationLogicSpy: ListImagesPresenterInput {
        
        // MARK: Method call expectations
        
        var presentFetchedImagesCalled = false
        var presentErrorCalled = false
        
        // MARK: Spied methods
        
        func presentAllImage(response: ListImages.ViewAll.Response) {
            presentFetchedImagesCalled = true
        }
        
        func presentError(error: Error) {
            presentErrorCalled = true
        }
    }
    
    class CommonWorkerSpy: CommonWorker {
        // MARK: Method call expectations
        
        var fetchImagesCalled = false
        
        // MARK: Spied methods
        
        override func fetchAllImages(userId: String, isOnline: Bool, completionHandler: @escaping (Result<ListImages.ViewAll.Response, CommonError>) -> Void) {
            fetchImagesCalled = true
            
            let uploadingImage = UploadingImage()
            uploadingImage.timestamp = 0
            uploadingImage.userId = "1"
            uploadingImage.fileName = "test.jpg"
            uploadingImage.fileType = "jpg"
            uploadingImage.imageData = Constant.defaultImage.jpegData(compressionQuality: 1.0)
            let uploadingImages = [uploadingImage]
            
            let uploadedImage = Dashboard.ViewUploaded.Response(timestamp: 0, userId: "1", fileName: "test.jpg", fileType: "jpg", imageData: Constant.defaultImage.jpegData(compressionQuality: 1.0))
            let uploadedImages = [uploadedImage]
            
            completionHandler(.success(ListImages.ViewAll.Response(uploadingImages: uploadingImages, uploadedImages: uploadedImages)))
        }
    }
    
    // MARK: - Tests
    
    func testFetchImagesShouldAskCommonWorkerToFetchAndPresenterToFormatResult() {
        // Given
        let listImagesPresentationLogicSpy = ListImagesPresentationLogicSpy()
        sut.presenter = listImagesPresentationLogicSpy
        let commonWorkerSpy = CommonWorkerSpy()
        sut.commonWorker = commonWorkerSpy

        // When
        let request = ListImages.ViewAll.Request(userId: "1", isOnline: true)
        sut.fetchAllImage(request: request)

        // Then
        XCTAssert(commonWorkerSpy.fetchImagesCalled, "Fetch images should ask common worker to fetch images")
        XCTAssert(listImagesPresentationLogicSpy.presentFetchedImagesCalled, "Fetch images should ask presenter to format images result")
    }
}
