@testable import SCGImageSync
import XCTest

class ListImagesPresenterTests: XCTestCase {
    // MARK: - Subject under test
    
    var sut: ListImagesPresenter!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupListOrdersPresenter()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupListOrdersPresenter() {
        sut = ListImagesPresenter()
    }
    
    // MARK: - Test doubles
    
    class ListImagesDisplayLogicSpy: ListImagesPresenterOutput {

        // MARK: Method call expectations
        
        var displayFetchedImagesCalled = false
        var displayErrorCalled = false
    
        // MARK: Argument expectations
        
        var viewModel: ListImages.ViewAll.ViewModel!
        var errorMessage: String!
        
        // MARK: Spied methods
        
        func displayAllImage(viewModel: ListImages.ViewAll.ViewModel) {
            displayFetchedImagesCalled = true
            self.viewModel = viewModel
        }
        
        func displayError(message: String) {
            displayErrorCalled = true
            self.errorMessage = message
        }
    }
    
    // MARK: - Tests

    func testPresentFetchedImagesShouldAskViewControllerToDisplay() {
        // Given
        let listImagesDisplayLogicSpy = ListImagesDisplayLogicSpy()
        sut.output = listImagesDisplayLogicSpy

        let uploadingImage = UploadingImage()
        uploadingImage.timestamp = 0
        uploadingImage.userId = "1"
        uploadingImage.fileName = "test.jpg"
        uploadingImage.fileType = "jpg"
        uploadingImage.imageData = Constant.defaultImage.jpegData(compressionQuality: 1.0)
        let uploadingImages = [uploadingImage]
        
        let uploadedImage = Dashboard.ViewUploaded.Response(timestamp: 0, userId: "1", fileName: "test.jpg", fileType: "jpg", imageData: Constant.defaultImage.jpegData(compressionQuality: 1.0))
        let uploadedImages = [uploadedImage]
        
        // When
        let response = ListImages.ViewAll.Response(uploadingImages: uploadingImages, uploadedImages: uploadedImages)
        sut.presentAllImage(response: response)

        // Then
        XCTAssert(listImagesDisplayLogicSpy.displayFetchedImagesCalled, "Presenting fetched images should ask view controller to display them")
    }
}
