@testable import SCGImageSync
import XCTest

class ListImagesViewControllerTests: XCTestCase {
    // MARK: - Subject under test
    
    var sut: ListImagesViewController!
    var window: UIWindow!
    
    // MARK: - Test lifecycle
    
    override func setUp() {
        super.setUp()
        window = UIWindow()
        setupListOrdersViewController()
    }
    
    override func tearDown() {
        window = nil
        super.tearDown()
    }
    
    // MARK: - Test setup
    
    func setupListOrdersViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "ListImagesViewController") as? ListImagesViewController
    }
    
    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }
    
    // MARK: - Test doubles
    
    class ListOrdersBusinessLogicSpy: ListImagesInteractorInput {
        
        // MARK: Method call expectations
        
        var fetchAllImagesCalled = false
        
        // MARK: Spied methods
        
        func fetchAllImage(request: ListImages.ViewAll.Request) {
            fetchAllImagesCalled = true
        }
    }
    
    class TableViewSpy: UITableView {
        // MARK: Method call expectations
        
        var reloadDataCalled = false
        
        // MARK: Spied methods
        
        override func reloadData() {
            reloadDataCalled = true
        }
    }
    
    // MARK: - Tests
    
    func testShouldFetchOrdersWhenViewDidAppear() {
        // Given
        let listImagesBusinessLogicSpy = ListOrdersBusinessLogicSpy()
        sut.interactor = listImagesBusinessLogicSpy
        loadView()
        
        // When
        sut.viewDidLoad()
        
        // Then
        XCTAssert(listImagesBusinessLogicSpy.fetchAllImagesCalled, "Should fetch images right after the view did load")
    }
    
    func testShouldDisplayFetchedOrders() {
        // Given
        let tableViewSpy = TableViewSpy()
        sut.tableView = tableViewSpy
        
        // When
        let formattedImages = [ListImages.ViewAll.ViewModel.FormattedImage(timestamp: 0, userId: "1", fileName: "test.jpg", fileType: "jpg", status: "Online", smallImage: Constant.defaultImage, largeImage: Constant.defaultImage)]
        let viewModel = ListImages.ViewAll.ViewModel(formattedImages: formattedImages)
        sut.displayAllImage(viewModel: viewModel)
        
        // Then
        XCTAssert(tableViewSpy.reloadDataCalled, "Displaying fetched persons should reload the table view")
    }
}
